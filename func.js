$('form.gallery-form').submit(function(e){

    $('div.gallery-container').empty();
    
    e.preventDefault();

    var galleryURL = $('input.gallery-form__input').val();

    if (galleryURL) {
        $.ajax({
            url: galleryURL,
            dataType: 'text'
            })
            .done(function( data ) {
                
                if (data) {
                    var galleryImages = $.parseJSON('{' + data + '}').galleryImages;
                    
                    if (galleryImages instanceof Array) {

                        var flexGrow, flexBasis;
                        var flexGrowMode  = 60;
                        var flexBasisMode = 144;
                        
                        $.each(galleryImages, function( i, val ){

                            flexGrow = Math.round((val.width * flexGrowMode / val.height) * 100) / 100;
                            flexBasis = Math.round((val.width * flexBasisMode / val.height) * 100) / 100;

                            $('div.gallery-container')
                                .append(
                                    $('<div class="gallery-container__item" style="flex-grow:'+flexGrow+'; -ms-flex-positive:'+flexGrow+'; flex-basis:'+flexBasis+'px; -ms-flex-preferred-size:'+flexBasis+'px;"><img src="'+val.url+'" alt=""></div>')
                                );    
                        });
                    }
                }
            })
            .fail(function() {
                showError('Please, enter the correct URL');
            });
    } else {
        showError('Please, enter URL');
    }
    
    return false;
});

function showError (text) {
    if (text) {
        $('div.gallery-container').append(
            $('<div>')
                .addClass('alert alert__danger')
                .text(text)
                .dblclick(function() {
                    $('input.gallery-form__input').val('https://don16obqbay2c.cloudfront.net/frontend-test-task/gallery-images.json')
                })
        );
    }
}